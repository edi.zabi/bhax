package binfa;

import java.io.PrintWriter;

public class LZWBinFa {
	
	public LZWBinFa() {
		fa = gyoker;
	}
	
	public void Bit(char b) {
		if(b == '0') {
			if(fa.nullasGyermek() == null) {
				Csomopont uj = new Csomopont('0');
				fa.ujNullasGyermek(uj);
				fa = gyoker;
			}
			else {
				fa = fa.nullasGyermek();
			}
		}
		else {
			if(fa.egyesGyermek() == null) {
				Csomopont uj = new Csomopont('1');
				fa.ujEgyesGyermek(uj);
				fa = gyoker;
			}
			else {
				fa = fa.egyesGyermek();
			}
		}
	}
	
	public void kiir() {
		melyseg = 0;
		kiir(gyoker, new PrintWriter(System.out));
	}
	
	public void kiir(PrintWriter write) {
		melyseg = 0;
		kiir(gyoker, write);
	}
	
	
	class Csomopont {
		public Csomopont(char betu) {
			this.betu = betu;
			balNulla = null;
			jobbEgy = null;
		};
		
		Csomopont nullasGyermek() {
			return balNulla;
		}
		
		Csomopont egyesGyermek() {
			return jobbEgy;
		}
		
		public void ujNullasGyermek(Csomopont gy) {
			balNulla = gy;
		}
		
		public void ujEgyesGyermek(Csomopont gy) {
			jobbEgy = gy;
		}
		
		public char getBetu() {
			return betu;
		}
		
		private char betu;
		private Csomopont balNulla = null;
		private Csomopont jobbEgy = null;
	};
	
	private Csomopont fa;
	private Integer melyseg;
	
	public void kiir(Csomopont elem, PrintWriter write) {
		if(elem != null) {
			++melyseg;
			kiir(elem.egyesGyermek(), write);
			for(int i = 0; i < melyseg; ++i) {
				write.print("---");
				write.print(elem.getBetu());
				write.print("(");
			    write.print(melyseg - 1);
			    write.println(")");
				kiir(elem.nullasGyermek(), write);
				--melyseg;
			}
		}
	}
	
	protected Csomopont gyoker = new Csomopont('/');
	int maxMelyseg = 0;

	public int getMelyseg() {
		melyseg = maxMelyseg;
		rmelyseg(gyoker);
		return maxMelyseg - 1;
	}
	
	public void rmelyseg (Csomopont elem) {
		if(elem != null) {
			++melyseg;
			if(melyseg > maxMelyseg) {
				maxMelyseg = melyseg;
			}
			rmelyseg(elem.egyesGyermek());
			rmelyseg(elem.nullasGyermek());
			--melyseg;
		}
	}
	
	public static void usage() {
		System.out.println("Usage: lzwtree in_file -o out_file");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		if(args.length != 4) {
			usage();
			System.exit(-1);;
		}
		String inFile = args[0];
		
		if(!"-o".equals(args[1])) {
			usage();
			System.exit(-2);
		}
		
		try {
			java.io.FileInputStream beFile =
		              new java.io.FileInputStream(new java.io.File(args[0]));
			java.io.PrintWriter kiFile = 
					new java.io.PrintWriter(
		            new java.io.BufferedWriter(
		            new java.io.FileWriter(args[2])));
		    byte[] b = new byte[1];
		    LZWBinFa binFa = new LZWBinFa();
		    
		    while(beFile.read(b) != -1) {
		    	if(b[0] == 0x0a) {
		    		break;
		    	}
		    }
		    
		    boolean kommentben = false;
		    
		    while(beFile.read(b) != -1) {
		    	if(b[0] == 0x3e) {
		    		kommentben = true;
		    		continue;
		    	}
		    	if(b[0] == 0x0a) {
		    		kommentben = false;
		    		continue;
		    	}
		    	if(kommentben) {
		    		continue;
		    	}
		    	if(b[0] == 0x4e) {
		    		continue;
		    	}
		    	
		    	for(int i = 0; i < 8 ; ++i) {
		    		if((b[0] & 0x80) != 0) {
		    			binFa.Bit('1');
		    		} 
		    		else
		    		{
		    			binFa.Bit('0');
		    		}
		    		b[0] <<= 1;
		    	}
		    }
		    
		    binFa.kiir(kiFile);
		    
		    kiFile.println("depth = " + binFa.getMelyseg());

		    kiFile.close();
		    beFile.close();

		} 
		catch (java.io.FileNotFoundException fnfException) {
		      fnfException.printStackTrace();
		} 
		catch (java.io.IOException ioException) {
		      ioException.printStackTrace();
		}
	}
}
