#include <stdio.h>
#include <curses.h>
#include <unistd.h>
#include <stdlib.h>

int main(void) { 
	WINDOW *ablak;
	ablak = initscr();

	int x = 0;
	int y = 0;

	int deltax = 1;
	int deltay = 1;

	int mx;
	int my;

	int xj = 0;
	int xk = 0;
	int yj = 0;
	int yk = 0;

	for(;;) {
		getmaxyx(ablak, my, mx);
		xj = (xj - 1) % mx;
		xk = (xk + 1) % mx;

		yj = (yj - 1) % my;
		yk = (yk + 1) % my;

		mvprintw (abs (yj + (my - yk)),
		abs (xj + (mx - xk)), "O");

		refresh ();
		usleep (100000);

		clear();
	}

	return 0;
}
