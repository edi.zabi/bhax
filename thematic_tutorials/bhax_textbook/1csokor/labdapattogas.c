#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int main(void) { 
	WINDOW *ablak;
	ablak = initscr();

	int x = 0;
	int y = 0;

	int deltax = 1;
	int deltay = 1;

	int mx;
	int my;

	for (;;) {
		getmaxyx (ablak, my, mx);
		mvprintw (y,x,"O");

		refresh();
		usleep(100000);

		//clear();

		x = x + deltax;
		y = y + deltay;

		if(x>=mx-1) {		//elerte-e a jobb oldalt?
			deltax = deltax * -1;
		}
		if(x<=0) { 		//elerte-e a bal oldalt?
			deltax = deltax * -1;
		}
		if(y<=0) {		//elerte-e a tetejet?
			deltay = deltay * -1;
		}
		if(y>=my-1) {		//elerte-e az aljat?
			deltay = deltay * -1;
		}
	}

	return 0;
}
