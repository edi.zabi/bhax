#include <stdio.h>

int main() {
	int a = 5;
	int b = 3;
	printf("Eredeti ertekek:");
	printf("\n a=%d",a);
	printf("\n b=%d",b);

	a = a ^ b;
	b = a ^ b;
	a = a ^ b;

	printf("\nCserelt ertekek:");
	printf("\n a=%d",a);
	printf("\n b=%d",b);
	printf("\n");
}
